﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lv7
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }

        private void Form2_Load(object sender, EventArgs e)
        {
            label1.Text = Igrac.prvi;
            label2.Text = Igrac.drugi;
            label3.Text = "";
            label4.Text = "";
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (Igrac.count % 2 == 0)
            {
                label4.Text = "";
                label3.Text = "X je na redu";
                button1.Text = "O";
                Igrac.x[0] = "O";
            }
            else
            {
                label4.Text = "O je na redu";
                label3.Text = "";
                button1.Text = "X";
                Igrac.x[0] = "X";
            }
            button1.Enabled = false;
            Igrac.count += 1;
            if (Igrac.x[0] == Igrac.x[1] && Igrac.x[1] == Igrac.x[2])
            {
                MessageBox.Show("Pobjednik je " + Igrac.x[0]);
                Application.Exit();
            }
            if (Igrac.x[0] == Igrac.x[4] && Igrac.x[4] == Igrac.x[8])
            {
                MessageBox.Show("Pobjednik je " + Igrac.x[0]);
                Application.Exit();
            }
            if (Igrac.x[0] == Igrac.x[3] && Igrac.x[3] == Igrac.x[6])
            {
                MessageBox.Show("Pobjednik je " + Igrac.x[0]);
                Application.Exit();
            }
            if (Igrac.count > 9)
            {
                MessageBox.Show("Izjednaceno");
                Application.Exit();
            }

        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (Igrac.count % 2 == 0)
            {
                label4.Text = "";
                label3.Text = "X je na redu";
                button2.Text = "O";
                Igrac.x[1] = "O";
            }
            else
            {
                label4.Text = "O je na redu";
                label3.Text = "";
                button2.Text = "X";
                Igrac.x[1] = "X";
            }
            button2.Enabled = false;
            Igrac.count += 1;
            if (Igrac.x[0] == Igrac.x[1] && Igrac.x[1] == Igrac.x[2])
            {
                MessageBox.Show("Pobjednik je " + Igrac.x[1]);
                Application.Exit();
            }
            if (Igrac.x[1] == Igrac.x[4] && Igrac.x[4] == Igrac.x[7])
            {
                MessageBox.Show("Pobjednik je " + Igrac.x[1]);
                Application.Exit();
            }
            if (Igrac.count > 9)
            {
                MessageBox.Show("Izjednaceno");
                Application.Exit();
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (Igrac.count % 2 == 0)
            {
                label4.Text = "";
                label3.Text = "X je na redu";
                button3.Text = "O";
                Igrac.x[2] = "O";
            }
            else
            {

                label4.Text = "O je na redu";
                label3.Text = "";
                button3.Text = "X";
                Igrac.x[2] = "X";
            }
            button3.Enabled = false;
            Igrac.count += 1;
            if (Igrac.x[0] == Igrac.x[1] && Igrac.x[1] == Igrac.x[2])
            {
                MessageBox.Show("Pobjednik je " + Igrac.x[0]);
                Application.Exit();
            }
            if (Igrac.x[2] == Igrac.x[4] && Igrac.x[4] == Igrac.x[6])
            {
                MessageBox.Show("Pobjednik je " + Igrac.x[2]);
                Application.Exit();
            }
            if (Igrac.x[2] == Igrac.x[5] && Igrac.x[5] == Igrac.x[8])
            {
                MessageBox.Show("Pobjednik je " + Igrac.x[2]);
                Application.Exit();
            }
            if (Igrac.count > 9)
            {
                MessageBox.Show("Izjednaceno");
                Application.Exit();
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (Igrac.count % 2 == 0)
            {
                label4.Text = "";
                label3.Text = "X je na redu";
                button4.Text = "O";
                Igrac.x[3] = "O";
            }
            else
            {
                label4.Text = "O je na redu";
                label3.Text = "";
                button4.Text = "X";
                Igrac.x[3] = "X";
            }
            button4.Enabled = false;
            Igrac.count += 1;
            if (Igrac.x[3] == Igrac.x[4] && Igrac.x[4] == Igrac.x[5])
            {
                MessageBox.Show("Pobjednik je " + Igrac.x[3]);
                Application.Exit();
            }
            if (Igrac.x[0] == Igrac.x[3] && Igrac.x[3] == Igrac.x[6])
            {
                MessageBox.Show("Pobjednik je " + Igrac.x[0]);
                Application.Exit();
            }
            if (Igrac.count > 9)
            {
                MessageBox.Show("Izjednaceno");
                Application.Exit();
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            if (Igrac.count % 2 == 0)
            {
                label4.Text = "";
                label3.Text = "X je na redu";
                button5.Text = "O";
                Igrac.x[4] = "O";
            }
            else
            {
                label4.Text = "O je na redu";
                label3.Text = "";
                button5.Text = "X";
                Igrac.x[4] = "X";
            }
            button5.Enabled = false;
            Igrac.count += 1;
            if (Igrac.x[1] == Igrac.x[4] && Igrac.x[4] == Igrac.x[7])
            {
                MessageBox.Show("Pobjednik je " + Igrac.x[1]);
                Application.Exit();
            }
            if (Igrac.x[3] == Igrac.x[4] && Igrac.x[4] == Igrac.x[5])
            {
                MessageBox.Show("Pobjednik je " + Igrac.x[3]);
                Application.Exit();
            }
            if (Igrac.x[0] == Igrac.x[4] && Igrac.x[4] == Igrac.x[8])
            {
                MessageBox.Show("Pobjednik je " + Igrac.x[0]);
                Application.Exit();
            }
            if (Igrac.x[2] == Igrac.x[4] && Igrac.x[4] == Igrac.x[6])
            {
                MessageBox.Show("Pobjednik je " + Igrac.x[2]);
                Application.Exit();
            }
            if (Igrac.count > 9)
            {
                MessageBox.Show("Izjednaceno");
                Application.Exit();
            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            if (Igrac.count % 2 == 0)
            {
                label4.Text = "";
                label3.Text = "X je na redu";
                button6.Text = "O";
                Igrac.x[5] = "O";
            }
            else
            {
                label4.Text = "O je na redu";
                label3.Text = "";
                button6.Text = "X";
                Igrac.x[5] = "X";
            }
            button6.Enabled = false;
            Igrac.count += 1;
            if (Igrac.x[5] == Igrac.x[4] && Igrac.x[4] == Igrac.x[3])
            {
                MessageBox.Show("Pobjednik je " + Igrac.x[5]);
                Application.Exit();
            }
            if (Igrac.x[2] == Igrac.x[5] && Igrac.x[5] == Igrac.x[8])
            {
                MessageBox.Show("Pobjednik je " + Igrac.x[2]);
                Application.Exit();
            }
            if (Igrac.count > 9)
            {
                MessageBox.Show("Izjednaceno");
                Application.Exit();
            }
        }

        private void button7_Click(object sender, EventArgs e)
        {
            if (Igrac.count % 2 == 0)
            {
                label4.Text = "";
                label3.Text = "X je na redu";
                button7.Text = "O";
                Igrac.x[6] = "O";
            }
            else
            {
                label4.Text = "O je na redu";
                label3.Text = "";
                button7.Text = "X";
                Igrac.x[6] = "X";
            }
            button7.Enabled = false;
            Igrac.count += 1;
            if (Igrac.x[0] == Igrac.x[3] && Igrac.x[3] == Igrac.x[6])
            {
                MessageBox.Show("Pobjednik je " + Igrac.x[0]);
                Application.Exit();
            }
            if (Igrac.x[6] == Igrac.x[7] && Igrac.x[7] == Igrac.x[8])
            {
                MessageBox.Show("Pobjednik je " + Igrac.x[6]);
                Application.Exit();
            }
            if (Igrac.x[6] == Igrac.x[4] && Igrac.x[4] == Igrac.x[2])
            {
                MessageBox.Show("Pobjednik je " + Igrac.x[6]);
                Application.Exit();
            }
            
            if (Igrac.count > 9)
            {
                MessageBox.Show("Izjednaceno");
                Application.Exit();
            }
        }

        private void button8_Click(object sender, EventArgs e)
        {
            if (Igrac.count % 2 == 0)
            {
                label4.Text = "";
                label3.Text = "X je na redu";
                button8.Text = "O";
                Igrac.x[7] = "O";
            }
            else
            {
                label4.Text = "O je na redu";
                label3.Text = "";
                button8.Text = "X";
                Igrac.x[7] = "X";
            }
            button8.Enabled = false;
            Igrac.count += 1;
            if (Igrac.x[6] == Igrac.x[7] && Igrac.x[7] == Igrac.x[8])
            {
                MessageBox.Show("Pobjednik je " + Igrac.x[7]);
                Application.Exit();
            }
            if (Igrac.x[1] == Igrac.x[4] && Igrac.x[4] == Igrac.x[7])
            {
                MessageBox.Show("Pobjednik je " + Igrac.x[7]);
                Application.Exit();
            }
            if (Igrac.count > 9)
            {
                MessageBox.Show("Izjednaceno");
                Application.Exit();
            }
        }

        private void button9_Click(object sender, EventArgs e)
        {
            if (Igrac.count % 2 == 0)
            {
                label4.Text = "";
                label3.Text = "X je na redu";
                button9.Text = "O";
                Igrac.x[8] = "O";
            }
            else
            {
                label4.Text = "O je na redu";
                label3.Text = "";
                button9.Text = "X";
                Igrac.x[8] = "X";
            }
            button9.Enabled = false;
            Igrac.count += 1;
            if (Igrac.x[2] == Igrac.x[5] && Igrac.x[5] == Igrac.x[8])
            {
                MessageBox.Show("Pobjednik je " + Igrac.x[8]);
                Application.Exit();
            }
            if (Igrac.x[6] == Igrac.x[7] && Igrac.x[7] == Igrac.x[8])
            {
                MessageBox.Show("Pobjednik je " + Igrac.x[8]);
                Application.Exit();
            }
            if (Igrac.x[0] == Igrac.x[4] && Igrac.x[4] == Igrac.x[8])
            {
                MessageBox.Show("Pobjednik je " + Igrac.x[8]);
                Application.Exit();
            }
          

            if (Igrac.count > 9)
            {
                MessageBox.Show("Izjednaceno");
                Application.Exit();
            }
        }

        private void label4_Click(object sender, EventArgs e)
        {

        }
    }

}

